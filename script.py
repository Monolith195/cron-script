import requests

def send_email(user, pwd, recipient, subject, body):
    import smtplib

    gmail_user = user
    gmail_pwd = pwd
    FROM = user
    TO = recipient if type(recipient) is list else [recipient]
    SUBJECT = subject
    TEXT = body

    message = """From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(FROM, TO, message)
        server.close()
        print ('successfully sent the mail')
    except:
        print ('failed to send mail')

url_list = {'1':'https://api.github.com/events',
	    '2':'http://httpbin.org/status/200',
	    '3':'http://httpbin.org/stats/404'}

recipients = ('az@trinitydigital.ru','monolith195@gmail.com')
for value in url_list:
    url = url_list.get(value)
    r = requests.get(url)
    status = r.status_code
    if status == 200:
        continue
    else:
        for recipient in recipients:
            send_email('Логин от почты','Пароль от почты',recipient,'Server status is not 200',url+' Alarm!!!')